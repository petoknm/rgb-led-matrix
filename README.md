# rgb-led-matrix
A library for driving HUB75(e) display matrices with STM32 microcontrollers.

## TODO
 - Bounds checking
 - Template parameter restrictions
 - Optimization of gpio output code (possibly more template parameters)

## Hardware setup
HUB75e pin descriptions:
 - R1 - red upper-half pixels
 - G1 - green upper-half pixels
 - B1 - blue upper-half pixels
 - R2 - red lower-half pixels
 - G2 - green lower-half pixels
 - B2 - blue lower-half pixels
 - [A-E] - row select pins
 - CLK - pixel clock
 - LAT - pixel latch
 - OE - output enable

## Implementation

### `pixel<Depth>`
 - represents an RGB pixel with a color depth of `Depth` of each channel
 - format: 0bRRRGGGBB (`Depth`==3), 0x0BGR (`Depth`==4), 0b0BBBBBGGGGGRRRRR (`Depth`==5)
 - methods:
     - `static pixel from(uint8_t r, uint8_t g, uint8_t b);`


### `image_bit_scan_line<X, N>`
 - represents two(`N`==1) or four(`N`==2) lines (upper-half and lower-half of the display/s)
 - can be directly shifted out as HUB75

### `image_bit_slice<X, Y, N>`
 - manages `Y/(2*N)` `image_bit_scan_line<X, N>`s
 - a slice through one of the color bits of the image
 - used for outputting HUB75

### `image<X, Y, Pixel, N>`
 - grid of pixels
 - inner representation optimized for fast HUB75 output (`image_bit_slice`)
 - methods:
     - `void set_pixel(uint8_t x, uint8_t y, Pixel p);`
     - `void clear();`

### `double_buffered_image<X, Y, Pixel, N>`
 - two images
 - one you can draw into while the second one is being displayed
 - once done with drawing you call `swap()` to swap the two images
 - methods:
     - `image<X, Y, Pixel, N>& draw_image();`
     - `const image<X, Y, Pixel, N>& display_image();`
     - `void swap();`

### `timer`
 - interface for the timer hardware
 - methods:
     - `void start_once(uint16_t us);`
     - `void stop();`

### `gpio`
 - interface for the gpio hardware
 - pinout:
```shell
(R1, G1, B1, R2, G2, B2) == (PIN_0..PIN_5)
# If two strings are used
(R3, G3, B3, R4, G4, B4) == (PIN_6..PIN_11)
```
 - methods:
     - `void send<X, Y, N>(const image_bit_slice<X, Y, N>& slice);`

### `hub75<X, Y, N, M, Pixel>`
 - describes the hardware connection to `N` strings of `M` displays of size `(X, Y)`
 - `N` can be only 1 or 2 (limited by the 16bit GPIO port)
 - internally creates a double buffered image of size `(M*X, N*Y)`
 - user supplied timer for HUB75 timing and refreshing
 - user supplied gpio for output
 - the displays are chained together
 ```shell
(stm32 -> display0 -> display1   -> ... -> displayM-1)
# If N == 2 second string is attached
(      -> displayM -> displayM+1 -> ... -> display2M-1)
```
 - methods:
     - `image<M*X, N*Y, Pixel, N>& draw_image();`
     - `void swap();`
     - `void interrupt_handler();`

## Mode of operation
 - hub75 sets up an interrupt driven state machine
 - interrupts perform the HUB75 output
 - you can do all the drawing in the main loop
 - you will loose a lot of CPU cycles (spent in the interrupts) to HUB75 output

## C interface

## Software setup
 - GPIO port to use (pinout in the `Hardware setup` and `gpio` section)
 - 16 bit timer with 1MHz counting frequency

## Requirements
For building:
 - GCC

For developing:
 - clang-format
 - doxygen
 - graphviz
 - moxygen
