#pragma once

#if defined __cplusplus
extern "C" {
#endif

void timer_interrupt_handler();
void example();

#if defined __cplusplus
}
#endif
