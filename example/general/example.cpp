#include "example.h"
#include "gpio.hpp"
#include "hub75.hpp"
#include "pixel.hpp"
#include "stm32f4xx_hal.h"
#include "timer.hpp"

extern "C" TIM_HandleTypeDef htim14;

timer t(htim14);
gpio  io(GPIOC);

hub75<64, 64, 1, 1, pixel<4>> display(t, io);

extern "C" void timer_interrupt_handler() { display.interrupt_handler(); }

extern "C" void example() {

    display.start();

    int frame = 0;

    while (true) {

        for (int i = 0; i < 64; i++) {
            for (int j = 0; j < 64; j++) {
                uint8_t v = i + j + frame;
                display.draw_image().set_pixel(j, i, pixel<4>::from(v, v, v));
            }
        }

        display.swap();
        frame++;
        HAL_Delay(10);
    }
}
